FROM germanwb/vlc:latest

RUN useradd streamer
WORKDIR /opt/code

ADD requirements.txt .
RUN pip3 install -r requirements.txt
ADD . .
RUN chown -R streamer /opt/code
USER streamer
CMD python3 run.py