import json, urllib.request, datetime, pause, os, subprocess
import logging
logging.basicConfig(level=logging.DEBUG)

shift = int(os.getenv('shift', 0))
sshift = datetime.timedelta(minutes=shift)
start_time = datetime.datetime.now() + sshift
logging.info(f'wait for {start_time}')
pause.until(start_time)
logging.info('start')
#url = 'http://101.ru/api/channel/getListServersChannel/199/channel?dataFormat=mobile'
path = f'data/{datetime.datetime.now().strftime("%Y-%m-%d")}/'

timeout = datetime.timedelta(minutes=int(os.getenv('timeout', 60)))
#f = urllib.request.urlopen(url)
#ff = f.read()
#dd = json.loads(ff)
#stream_url = dd.get('result')[0].get('urlStream')
stream_url = 'https://stream85.polskieradio.pl/live/radio_dzieciom.sdp/playlist.m3u8'
logging.info(f'url {stream_url}')
name = f'{datetime.datetime.now().strftime("%Y-%m-%d-%H_%M_%S")}.mp3'
sout = r'--sout "#transcode{acodec=mp3,ab=128,channels=2,samplerate=44100}:std{access=file,mux=raw,dst='+\
       f"{path}{name}"+'}"'
run = f'cvlc {stream_url} {sout}'


logging.info(f'create dir {path}')
os.system(f'mkdir -p {path}')
logging.info(f'start stream, {run}')
process = subprocess.Popen(run, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
logging.info('whait for stop')
print(run)
stop = datetime.datetime.now()+timeout
logging.info(f'stop time {stop.strftime("%H_%M_%S")}')
pause.until(stop)
logging.info('try stop process')
process.terminate()
logging.info('wait stop')
process.wait()
exit(0)
