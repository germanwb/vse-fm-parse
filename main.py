import time, os
import logging
from yaml import load
try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M')

'''VARS'''
config_file = 'config.yaml'
'''/VARS'''


class Grabber:
    import datetime
    from selenium import webdriver
    start = datetime.datetime
    end = datetime.datetime
    zero_delta = datetime.timedelta(hours=0)
    pattern = ''
    driver = webdriver.Chrome('/usr/bin/chromedriver')

    def __init__(self, start_date_str, end_date_str, pattern, token, root_url):
        self.pattern = pattern
        self.__init_timers__(start_date=start_date_str, end_date=end_date_str)
        self.token = token
        self.root_url = root_url
        self.__login__()

    def __init_timers__(self, start_date, end_date):
        logging.debug(f'start init timers {start_date} {end_date} ')
        self.start = self.datetime.datetime.strptime(start_date, "%Y-%m-%d %H")
        self.end = self.datetime.datetime.strptime(end_date, "%Y-%m-%d %H")
        self.delta = (self.end - self.start)

    def _daterange_(self):
        logging.debug('init generator')

        logging.debug(f'delta = {self.delta} ')
        logging.debug(f'zero_delta = {self.zero_delta}')
        while self.zero_delta <= self.delta:
            self.zero_delta = self.zero_delta + self.datetime.timedelta(hours=1)
            logging.debug(f'dif zero_delta = {self.zero_delta}')
            yield self.start + self.zero_delta

    def list_of_days(self):
        logging.debug(f'start list of day {self.start} {self.end} ')
        arr = []
        for single_date in self._daterange_():
            logging.debug(f'append date {single_date}')
            arr.append({'url': single_date.strftime(self.pattern),
                        'time': single_date.strftime("%H:00")})
        return arr

    def __login__(self):
        self.driver.get(self.root_url)
        key_box = self.driver.find_element_by_class_name('checkKey')
        key_box.clear()
        key_box.send_keys(self.token)
        self.driver.find_element_by_class_name('checkCode').click()

    def start_parse(self):
        for link in self.list_of_days():
            self.driver.get(self.root_url+link['url'])
            radio_list = self.driver.find_element_by_css_selector(
                'body > div.main > div.center > div.window > div.player > div.playlists')
            for song in radio_list.find_elements_by_class_name('program'):
                if song.text == link['time']+'Эфир радиостанции':
                    song.find_element_by_css_selector('#link').click()
                    self.driver.find_element_by_class_name('url_save').click()
            time.sleep(10)
        self.driver.close()


def start():
    data = dict()
    if os.path.isfile(config_file):
        file = open(config_file)
        data = load(file, Loader=Loader)
        print(data)
    mygrabber = Grabber(start_date_str=data['config']['start_date'],
                        end_date_str=data['config']['end_date'],
                        pattern=data['config']['pattern'],
                        token=data['config']['token'],
                        root_url=data['config']['root_url'])
    mygrabber.start_parse()


if __name__ == '__main__':
    try:
        start()
    except KeyboardInterrupt:
        print("Canceling")
        exit(0)
